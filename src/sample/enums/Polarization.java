package sample.enums;

import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

public enum Polarization {
    Angle90(0),
    Angle0(1),
    Angle45(2),
    Angle135(3);

    private final int PolarizationCode;

    private Polarization(int polarizationCode) {
        this.PolarizationCode = polarizationCode;
    }


    public int getPolarizationCode() {
        return this.PolarizationCode;
    }

    public static Polarization getValue(String value) {
        HashMap<Integer, Polarization> polarizationMap = new HashMap<Integer, Polarization>();
        polarizationMap.put(0, Angle90);
        polarizationMap.put(1, Angle0);
        polarizationMap.put(2, Angle45);
        polarizationMap.put(3, Angle135);
        return polarizationMap.get(Integer.parseInt(value));
    }
}
