package sample.enums;

import java.util.HashMap;

public enum Basis {
    Plus(0),
    Cross(1);

    private final int basisCode;

    private Basis(int basisCode) {
        this.basisCode = basisCode;
    }

    public int getBasisCode() {
        return this.basisCode;
    }

    public static Basis getBasis(String value) {
        HashMap<Integer, Basis> basisHashMap = new HashMap<>();
        basisHashMap.put(0, Plus);
        basisHashMap.put(1, Cross);
        return basisHashMap.get(Integer.parseInt(value));
    }
}
