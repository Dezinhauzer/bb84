package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import sample.enums.*;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.StringJoiner;

public class Controller {
    public CheckBox CheckBoxEva;
    public Button ButtonGenerateKey;
    public TextField TextFieldKey;
    public TextField ResultSequenceBob;
    public Label ResultDetected;
    private Basis[] allBasis;
    private Polarization[] allPolarization;
    private Basis[] basisAlice;
    private Basis[] basisBob;
    private Polarization[] polarizationsAlice;
    private Polarization[] polarizationsBob;

    public RadioButton RadioButtonAuto;
    public RadioButton RadioButtonNoAuto;
    public TextField TextBoxEva;
    public TextField TextBoxSequenceAlice;
    public TextField TextBoxSequenceBob;
    private Integer countFoton;

    public TextField TextFieldCountFoton;

    private void AlertMessageBox(String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Error");
        alert.setHeaderText("Произошло неведомое");
        alert.setContentText(text);
        alert.showAndWait().ifPresent(rs -> {
        });
    }

    public void generate(ActionEvent actionEvent) {
        try {
            TextBoxSequenceAlice.setText("");
            TextBoxSequenceBob.setText("");
            TextFieldKey.setText("");
            countFoton = Integer.parseInt(TextFieldCountFoton.getText());
            Random rnd = new Random();
            if (RadioButtonAuto.isSelected()) {

                basisAlice = new Basis[countFoton];
                basisBob = new Basis[countFoton];
                polarizationsAlice = new Polarization[countFoton];
                polarizationsBob = new Polarization[countFoton];

                if (allBasis == null || allPolarization == null) {
                    allBasis = Basis.values();
                    allPolarization = Polarization.values();
                }

                // Гененрируем последовательность базисов и фотонов Алисы.
                for (int i = 0; i < countFoton; i++) {
                    basisAlice[i] = allBasis[rnd.nextInt(2)];
                    polarizationsAlice[i] = allPolarization[rnd.nextInt(2) + (2 * basisAlice[i].getBasisCode())];
                }

                for (int i = 0; i < countFoton; i++) {
                    basisBob[i] = allBasis[rnd.nextInt(2)];
                }

                StringJoiner joinerAlice = new StringJoiner(",");
                StringJoiner joinerBob = new StringJoiner(",");
                for (int i = 0; i < countFoton; i++) {
                    joinerBob.add(String.valueOf(basisBob[i].getBasisCode()));
                    joinerAlice.add(String.valueOf(polarizationsAlice[i].getPolarizationCode()));
                }
                TextBoxSequenceBob.setText(joinerBob.toString());
                TextBoxSequenceAlice.setText(joinerAlice.toString());

                TextBoxSequenceAlice.setDisable(true);
                TextBoxSequenceBob.setDisable(true);
            } else if (RadioButtonNoAuto.isSelected()) {
                for (int i = 0; i < countFoton; i++) {
                    basisAlice[i] = allBasis[rnd.nextInt(2)];
                }

                TextBoxSequenceAlice.setDisable(false);
                TextBoxSequenceBob.setDisable(false);
            }

        } catch (NumberFormatException ex) {
            AlertMessageBox(ex.getMessage());
        }
    }


    public void Selected(ActionEvent actionEvent) {
        if (((RadioButton) actionEvent.getTarget()).getId().equals(RadioButtonAuto.getId())) {
            RadioButtonAuto.setSelected(true);
            RadioButtonNoAuto.setSelected(false);
        } else {
            RadioButtonNoAuto.setSelected(true);
            RadioButtonAuto.setSelected(false);
        }
    }

    public void CheckBoxEvaAction(ActionEvent actionEvent) {
        TextBoxEva.setDisable(!TextBoxEva.isDisable());
    }

    public void generateKey(ActionEvent actionEvent) throws Exception {
        try {
            String[] Bob = checkValuePolarization(TextBoxSequenceBob.getText(), "Bob");
            String[] Alice = checkValuePolarization(TextBoxSequenceAlice.getText(), "Alice");
            Boolean bobIsOk = true;

            for (int i = 0; i < countFoton; i++) {
                polarizationsAlice[i] = Polarization.getValue(Alice[i]);
                basisBob[i] = Basis.getBasis(Bob[i]);
            }
            Polarization[] copyPolarizationsAlice = Arrays.copyOf(polarizationsAlice, polarizationsAlice.length);
            if (CheckBoxEva.isSelected()) {
                Arrays.stream(TextBoxEva.getText().split(",")).mapToInt(Integer::parseInt).forEach(el -> copyPolarizationsAlice[el] = Polarization.Angle90);
            }

            StringJoiner joinerKey = new StringJoiner(",");
            StringJoiner joinerAllKey = new StringJoiner(",");
            for (int i = 0; i < countFoton; i++) {
                int code = copyPolarizationsAlice[i].getPolarizationCode();
                if (code != polarizationsAlice[i].getPolarizationCode()) {
                    bobIsOk = false;
                }
                String resultMEssage = code > 1 ? String.valueOf(code - 2) : String.valueOf(code);
                if (basisBob[i] == basisAlice[i]) {
                    joinerAllKey.add(String.valueOf(code));
                    joinerKey.add(resultMEssage);
                } else {
                    joinerAllKey.add(String.valueOf(2 * basisBob[i].getBasisCode() + Integer.parseInt(resultMEssage)));
                    joinerKey.add("_");
                }
            }
            ResultSequenceBob.setText(joinerAllKey.toString());
            TextFieldKey.setText(joinerKey.toString());
            ResultDetected.setText(bobIsOk ? "Верен" : "Не верен");
        } catch (Exception ex) {
            AlertMessageBox(ex.getMessage());
        }
    }

    private String[] checkValuePolarization(String polarizationString, String who) throws Exception {
        String[] fotons = polarizationString.split(",");
        if (fotons.length != countFoton) {
            throw new Exception("В последовательности: " + who + "\nДолжно быть: " + countFoton + "\nСейчас есть: " + fotons.length);
        }
        return fotons;
    }

    public void ButtonClose(ActionEvent actionEvent) {
        Platform.exit();
        System.exit(0);
    }
}
